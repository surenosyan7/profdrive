import React, { Component, Fragment } from 'react'
import './home.css'
import PatetCard from '../patetCard/patetCard';
import Comments from '../comments/comments';
import Header from '../header/header.js';

class Home extends Component {
  render() {
    return (
        <Fragment>
          <Header/>
          <div className='info'>
                <div className='inner-info'>
                  <h2 className='info-h'>Վարորդական իրավունքի տեսական և գործնական քննություններին պատրաստվիր Պրոֆդրայվ Ավտոդպրոցում</h2>
                  <div className='paragraphs'>
                    <p>Պրոֆդրայվ ավտոդպրոցում ձեռք կբերեք բոլոր հմտություններ, որ անհրաժեշտ են անվտանգ ավտովարման համար:</p>
                    <p>Անցնելով մեր դասընթացները՝ դիմորդները ձեռք են բերում երթևեկության անվտանգության հմտություններ և վարելու պրակտիկայի ամուր հիմք, որի շնորհիվ վարորդները իրենց վստահ և ապահով են զգում բոլոր իրավիճակներում:</p>
                    <p>Մեր առաքելությունն է յուրաքանչյուր դիմորդի դարձնել վստահ և պատասխանատու վարորդ՝ այդպիսով երթևեկությունը դարձնելով անվտանգ և ապահով:</p>
                  </div>
                  <h3 className='lower-header'>Ի՞ՆՉՆ Է ՊՐՈՖԴՐԱՅՎ-Ը ԴԱՐՁՆՈՒՄ ԼԱՎԱԳՈՒՅՆԸ</h3>
                  <div className='lis'>
                    <ul>
                      <li>Պրոֆեսիոնալ ուսուցիչներ</li>
                      <li>Անհատական մոտեցում</li>
                      <li>Տեսական արժեքավոր գիտելիքներ</li>
                      <li>Փորձառություն</li>
                      <li>Մատչելի և ճկուն փաթեթներ</li>
                    </ul>
                  </div>
                </div>
          </div>
          <div className='patetner'>
              <div className='patetner-control-div'>
                <h3 className='lower-header' style={{textAlign: 'center'}}>ՑԱՆԿԱՆՈ՞ՒՄ ԵՔ ՊՐԱԿՏԻԿ ԴԱՍԵՐ</h3>
                <h1 className='header'>Փաթեթներ</h1>
                <div className='patetner-variants'>
                  <PatetCard cardName={"Beginner"} hours={"1"} price={"4000"}/>
                  <PatetCard cardName={"Start"} hours={"5"} price={"25000"}/>
                  <PatetCard cardName={"Standard"} hours={"10"} price={"50000"}/>
                </div>
              </div>
          </div>
          <Comments />
        </Fragment>
    )
  }
}

export default Home