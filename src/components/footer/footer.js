import { faFacebook, faGoogle, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component, Fragment } from 'react'
import './footer.css'

class Footer extends Component {
  render() {
    return (
      <Fragment>
        <footer>
            <div className='footer-links'>
                <div className='footer-links-div-1'>
                    <div className='footer-div-1'>
                      <a href='/contact'>ՀԵՏԱԴԱՐՁ ԿԱՊ</a>
                      <a href='/about-us'>ՄԵՐ ՄԱՍԻՆ</a>
                    </div>
                    <div className='footer-div-2'>
                      <a href='/qnnakan-harcer'>ՔՆՆԱԿԱՆ ՀԱՐՑԵՐ ԸՍՏ ԹԵՄԱՆԵՐԻ</a>
                      <a href='/porcnakan-tester'>ՓՈՐՁՆԱԿԱՆ ԹԵՍՏԵՐ</a>
                      <a href='/hogebanakan-tester'>ԱՎՏՈՎԱՐՄԱՆ ՀՈԳԵԲԱՆԱԿԱՆ ԹԵՍՏ</a> 
                    </div>
                    <div className='footer-div-3'>
                      <a href='/video-daser'>ՎԻԴԵՈ ԴԱՍԵՐ</a>
                      <a href='/ertevekutyan-kannoner'>ՃԱՆԱՊԱՐՀԱՅԻՆ ԵՐԹԵՎԵԿՈՒԹՅԱՆ ԿԱՆՈՆՆԵՐ</a>
                    </div>
                </div>
                <div className='footer-links-div-2'>
                    <div className='social-info-foot'>
                        <a href='https://www.facebook.com/profdriveavtodproc/'><FontAwesomeIcon icon={faFacebook} /></a>
                        <a href='https://twitter.com/profdriveschool'><FontAwesomeIcon icon={faTwitter} /></a>
                        <a href='https://www.instagram.com/profdrive_avtodproc/'><FontAwesomeIcon icon={faInstagram} /></a>
                        <a href='https://g.page/profdriveavtodproc'><FontAwesomeIcon icon={faGoogle} /></a>
                        <a href='tel:+37498788738'>+374 (98) 78 87 38</a>
                    </div>
                </div>
            </div>
            
        </footer>
      </Fragment>
    )
  }
}

export default Footer