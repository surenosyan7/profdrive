import React, { Component, Fragment } from 'react'
import { Parallax } from 'react-parallax';
import Logo from '../../images/menupf.png';
import bgimage from '../../images/hero_2.jpg'
import './header.css';

class Header extends Component {
  render() {
    return (
      <Fragment>
        <Parallax bgImage={bgimage} className={'w-100 h-100 parralax'}>
          <header>
            <div className='opacity-div'></div>
              <div className='header-control-div'>
                <div className='div-1'>
                  <img src={Logo} alt='logo'></img>
                  <p class="mb-5 lead text-white">Ձեռք բեր տեսական և պրակտիկ վարման հմտություններ Profdrive ավտոդպրոցում:<br/> Մենք սպասում ենք հենց քեզ:</p>
                  <a href='/order/create' className="header-link">Գրանցվել</a>
                </div>
                <div className='div-2'>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Քննությունների պատրաստում</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Վարման հմտություններ</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Քաղաքային վարում</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Կայանման բոլոր ձեւեր</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Տեսական գիտելիքներ</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Գործնական վարում</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Վարորդների վերապատրաստում</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Վարում ավտոմայրուղիներում</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Գիշերային վարում</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Վթարային իրավիճակներում ճիշտ կողմնորոշվելու հմտություններ</p>
                  <p className='w-75' style={{margin: "0.25rem", marginLeft: "15px"}}>✔ Ավտոմատ եւ մեխանիկական փոխանցումատուփ</p>
                </div>
            </div>
          </header>
        </Parallax>
      </Fragment>
    )
  }
}

export default Header