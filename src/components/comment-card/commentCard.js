import React, { Component, Fragment } from 'react'
import './commentCard.css'
import profilePicture from '../../images/no-image-profile.png'

class CommentCard extends Component {
  render() {
    return (
      <Fragment>
        <div className='comment-card'>
          <div className='profile-picture'>
            <img alt='profile' src={profilePicture}></img>
          </div>
          <p className='comment-text'>{this.props.commentText}</p>
          <p>{this.props.username}</p>
        </div>
      </Fragment>
    )
  }
}

export default CommentCard