import React, { Component, Fragment } from 'react'
import './comments.css'
import Carousel  from 'react-bootstrap/Carousel';
import CommentCard from '../comment-card/commentCard';

class Comments extends Component {
  render() {
    var comments = 
      {
        1: "“Միանշանակ խորհուրդ եմ տալիս Profdrive ավտոդպրոցը։ Անչափ շնորհակալ եմ Կարենին, գրագետ ուսուցման բարձր գիտելիքները ու հմտությունները փոխանցելու համար։ Ստացել եմ մեկ անգամից և տեսականը և գործնականը ու առանց  որևէ սխալի։ Եվ երբ հետ եմ նայում և ամփոփում եմ այսօրվա օրը, ուզում եմ հասկանալ, թե որ՞ն է իմ ինքնավստահ և բավարարված զգացողության պատճառը, հասկանում եմ որ գործ եմ ունեցել մի մարդու հետ, ով ամենայն պատասխանատվությամբ է վերաբերվում իր աշխատանքին և դու դադարում ես հակառակ մոտեցում ցուցաբերել։Լավագույն ավտոդպրոց Profdrive.”",
        2: "“Выражаю огромную благодарность моему инструктору по вождению. Спасибо вам за вашу работу. Желаю автошколе успехов и дальнейшего развития”",
        3: "“Очень понравилось заниматься в этой автошколе у Карена. Он пунктуальный, хорошо объясняет, старается научить именно как правильно, а не просто чтобы сдать экзамен. Всем советую, не пожалеете !”"
      }
    var username =
      {
        1: 'Narine Vardikyan',
        2: 'Astxik Gevorgyan',
        3: 'Armine Torosian'
      }
    return (
      <Fragment>
        <div className='comments-section'>
            <div className='name-container'>
                <h3 className='lower-header' style={{textAlign: 'center'}}>Ի՞ՆՉ ԵՆ ԽՈՍՈՒՄ ՄԵՐ ՄԱՍԻՆ</h3>
                <h1 className='header'>Մեկնաբանություններ</h1>
            </div>
            <div className='slider-container'>
                <Carousel className='slider' touch>
                    <Carousel.Item>
                        <CommentCard commentText={comments[1]} username={username[1]} />
                    </Carousel.Item>
                    <Carousel.Item>
                        <CommentCard commentText={comments[2]} username={username[2]} />
                    </Carousel.Item>
                    <Carousel.Item>
                        <CommentCard commentText={comments[3]} username={username[3]} />
                    </Carousel.Item>
                </Carousel>
            </div>
        </div>
      </Fragment>
    )
  }
}

export default Comments;