import React, { Component, Fragment } from 'react'
import './patetCard.css'
import logo from '../../images/logo.png'

class PatetCard extends Component {
  render() {
    return (
      <Fragment>
            <div className='patet-card'>
                <div className='card-icon'>
                    <img alt='logo' src={logo} width={"100%"} ></img>
                </div>
                <div className='card-info'>
                    <h4 className='card-name'>{this.props.cardName}</h4>
                    <p className='card-price'>{this.props.hours}  Ժամ - {this.props.price} AMD</p>
                </div>
                <div className='hover-div'>
                    <p>{this.props.cardName}</p>
                </div>
            </div>
      </Fragment>
    )
  }
}

export default PatetCard