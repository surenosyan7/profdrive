import { faFacebook, faGoogle, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component, Fragment } from 'react'
import Logo from '../../images/logo1.png';
import './nav.css';

class Nav extends Component {
    state = {
        sideMenuDisplay: "none"
    }
    openSideMenu = () => {
        this.setState({
            sideMenuDisplay: "flex"
        })
    }
    closeSideMenu = () => {
        this.setState({
            sideMenuDisplay: "none"
        })
    }
  render() {
    return (
        <Fragment>
            <nav>
                <div className='control-div'>
                    <div className='logo'>
                        <a className='logo-picture' href='/'>
                            <img src={Logo} alt="logo"></img>
                        </a>
                    </div>
                    <div className='links'>
                        <a className='tester' href='/qnnakan-tester'>Թեստեր</a>
                        <div className='tester-select'>
                            <ul className='menu'>
                                <li><a href='/video-daser'>Վիդեո դասեր</a></li>
                                <li><a href='/qnnakan-harcer'>Քննական հարցեր ըստ թեմաների</a></li>
                                <li><a href='/porcnakan-tester'>Փորձնական թեստեր</a></li>
                                <li><a href='/hoqebanakan-tester'>Ավտովարման հոգեբանական թեստ</a></li>
                            </ul>
                        </div>
                        <a href='/site/login'>Մուտք</a>
                    </div>
                    <div className='social-info'>
                        <a href='https://www.facebook.com/profdriveavtodproc/'><FontAwesomeIcon icon={faFacebook} /></a>
                        <a href='https://twitter.com/profdriveschool'><FontAwesomeIcon icon={faTwitter} /></a>
                        <a href='https://www.instagram.com/profdrive_avtodproc/'><FontAwesomeIcon icon={faInstagram} /></a>
                        <a href='https://g.page/profdriveavtodproc'><FontAwesomeIcon icon={faGoogle} /></a>
                        <a href='tel:+37498788738'>+374 (98) 78 87 38</a>
                    </div>
                </div>
                <div className='side-menu-icon h-100 justify-content-center align-items-center'>
                    <FontAwesomeIcon onClick={this.openSideMenu} className='h2 m-0' icon={faBars}/>
                </div>
                <div style={{display: this.state.sideMenuDisplay}} className='side-menu'>
                    <a className='tester' href='/qnnakan-tester'>Թեստեր</a>
                    <div className='tester-select-type-menu'>
                        <ul className='menu'>
                            <li><a href='/video-daser'>Վիդեո դասեր</a></li>
                            <li><a href='/qnnakan-harcer'>Քննական հարցեր ըստ թեմաների</a></li>
                            <li><a href='/porcnakan-tester'>Փորձնական թեստեր</a></li>
                            <li><a href='/hoqebanakan-tester'>Ավտովարման հոգեբանական թեստ</a></li>
                        </ul>
                    </div>
                    <a href='/site/login'>Մուտք</a>
                    <FontAwesomeIcon onClick={this.closeSideMenu} style={{top: "3%", right: "10%"}} className='h2 position-absolute' icon={faTimes}/>
                </div>
            </nav>
        </Fragment>
    )
  }
}

export default Nav